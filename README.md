# Python-flask-boilerplate

## Overview
파이선 플라스크 보일러플레이트

## Technologies
Project is created with:

    * Python: 3.7.2
    * Mysql: 5
    * Flask 
    * SQLAlchemy
    * Marshmallow
    * Pytest & Pytest-cov

## Installation
```
pyenv install 3.7.2
pyenv virtualenv 3.7.2 [환경명]
pip install -r requriements.txt
export SERVICE_ENV=local
python run_debug.py
```

## Pytest
run pytest  
```pytest```

run pytest coverage  
``` pytest --cov-report term --cov=project tests/```
   
## Style Guide
- [PEP8](https://www.python.org/dev/peps/pep-0008/) 
- [Google Python style Guide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md) 
