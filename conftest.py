import os
import pytest
import json
from datetime import datetime
from project import databases
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from project.app import create_app
from project.databases import Base
from project.env import config_by_name
from project.models import User, UserLastAccessTime, UserAuthToken, UserDevice, DeviceGroupBlockPolicyStatus, \
    UserRestrictionPolicy


@pytest.fixture(scope='session')
def app():
    config_name = config_by_name['test']
    app = create_app(config_name)
    app_context = app.app_context()
    app_context.push()
    yield app
    app_context.pop()


@pytest.fixture(scope='session')
def init_db():
    Engine = create_engine(config_by_name['test'].TEST_DB_URI, convert_unicode=True, pool_recycle=900)
    meta = Base.metadata
    meta.bind = Engine
    meta.create_all()
    yield Engine
    meta.drop_all()
    Engine.dispose()


@pytest.fixture(scope='session')
def client(app):
    return app.test_client()


@pytest.fixture(scope='session')
def session(init_db, client):
    databases.WriteSession = sessionmaker(autocommit=False, autoflush=False, bind=init_db)
    databases.ReadSession = sessionmaker(autocommit=False, autoflush=False, bind=init_db)
    session = databases.WriteSession()
    yield session
    session.rollback()
    session.close()


def read_data_set(fname):
    with open('{}/tests/fixture/dataset/{}.json'.format(os.path.dirname(os.path.realpath(__file__)), fname), 'r') as file:
        return json.load(file)


def read_input_set(fname):
    with open('{}/tests/fixture/inputcase/{}.json'.format(os.path.dirname(os.path.realpath(__file__)), fname), 'r') as file:
        return json.load(file)


def read_output_set(fname):
    with open('{}/tests/fixture/outputcase/{}.json'.format(os.path.dirname(os.path.realpath(__file__)), fname), 'r') as file:
        return json.load(file)


@pytest.fixture(scope='function')
def user(session):
    data = read_data_set('active_user')
    user = User(id=data['user']['id'], name=data['user']['name'], noti_key_c2dm='')

    user_last_access_time = UserLastAccessTime(uid=data['user']['id'], access_time=datetime.now())

    user_token_data = data['user_auth_token']
    user_auth_token = UserAuthToken(uid=user_token_data['uid'], token=user_token_data['token'],
                                    policy_id=user_token_data['policy_id'])

    user_device_data = data['user_device']
    user_device = UserDevice(uid=user_device_data['uid'],
                             source=user_device_data['source'],
                             udid_str=user_device_data['udid_str'],
                             device=user_device_data['device'],
                             device_model=user_device_data['device_model'],
                             latest_version=user_device_data['latest_version'],
                             date=datetime.now(),
                             created_at=datetime.now(),
                             status=0)

    device_group_block_policy_status = DeviceGroupBlockPolicyStatus(udid=data['device_group_block_policy_status']['udid'],
                                                                    policy_id=data['device_group_block_policy_status']['policy_id'],
                                                                    created_at=datetime.now(),
                                                                    modified_at=datetime.now())
    for row in data['user_restriction_policy']:
        session.add(UserRestrictionPolicy(**row))
    session.add(user)
    session.add(user_last_access_time)
    session.add(user_auth_token)
    session.add(user_device)
    session.add(device_group_block_policy_status)
    session.commit()
    yield
    session.query(User).delete()
    session.query(UserLastAccessTime).delete()
    session.query(UserAuthToken).delete()
    session.query(UserDevice).delete()
    session.query(DeviceGroupBlockPolicyStatus).delete()
    session.commit()


def header():
    data = read_data_set('active_user')
    return {'X-Quicket-Auth-Token': data['user_auth_token']['token']}

