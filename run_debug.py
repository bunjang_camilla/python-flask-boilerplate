from project.app import create_app
from project.env import current_config

app = create_app(current_config)
if __name__ == '__main__':
    app.run()