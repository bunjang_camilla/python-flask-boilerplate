from conftest import header, read_input_set, read_output_set


def test_get_user_detail(client, user):
    resp = client.get('/users', headers=header())
    assert resp.status_code == 200
    assert list(resp.get_json()['data'].keys()) == ['id', 'name']


def test_missing_key_to_update_user_detail(client, user):
    resp = client.patch('/users', headers=header(), json=dict(name='name'))
    assert resp.status_code == 422


def test_update_user_detail(client, user):
    input_data = read_input_set('new_user_detail')
    resp = client.patch('/users', headers=header(), json=input_data)
    assert resp.status_code == 201
    result = resp.get_json()['data']
    output_data = read_output_set('new_user_detail')
    assert result['name'] == output_data['name']
    assert result['description'] == output_data['description']


