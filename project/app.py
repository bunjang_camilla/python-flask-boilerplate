import sys
import logging
from flask import Flask
from flask_login import LoginManager
from flask_restful import Api
from flask_swagger import swagger
from project.api.authentication import Anonymous
from project.api.authentication.login import *
from requests.adapters import HTTPAdapter

from project.api.sample.users import UserDetail
from project.databases import db
from project.env import config_by_name
from project.models import User


def create_app(config_name=None):
    app = Flask(__name__)
    if not config_name:
        config_name = config_by_name['local']
    app.config.from_object(config_name)
    app.register_blueprint(db)
    app.http_adapter = HTTPAdapter(pool_connections=app.config['HTTP_POOL_CONNECTIONS'],
                                   pool_maxsize=app.config['HTTP_POOL_MAXSIZE'])

    if app.config['ENV'] in ('production', 'staging'):
        import sentry_sdk
        from sentry_sdk.integrations.flask import FlaskIntegration
        sentry_sdk.init(
            dsn=app.config['SENTRY_DSN'],
            integrations=[FlaskIntegration()]
        )

        if app.config['ENV'] == 'production':
            logging.getLogger('sqlalchemy.engine').setLevel(logging.DEBUG)
            logging.getLogger('sqlalchemy.dialects').setLevel(logging.DEBUG)
            logging.getLogger('sqlalchemy.pool').setLevel(logging.DEBUG)
            logging.getLogger('sqlalchemy.orm').setLevel(logging.WARN)
            logging.basicConfig(level=logging.DEBUG,
                                stream=sys.stdout,
                                format='%(asctime)s %(levelname)s %(name)s %(message)s')

    login_manager = LoginManager()
    login_manager.anonymous_user = Anonymous
    login_manager.login_view = 'login'
    login_manager.login_message = 'login_message (what?)'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return db.query(User).filter(User.id == user_id).first()

    @app.route('/ping')
    def health_check():
        return jsonify(dict(ok='ok'))

    api = Api(app)
    api.add_resource(UserDetail, '/users')

    if app.config['ENV'] != 'production':
        @app.route('/spec')
        def spec():
            swag = swagger(app)
            swag['description'] = 'Flask boilerplate API'
            swag['info']['version'] = '1.0'
            swag['info']['title'] = 'Flask boilerplate API'
            return jsonify(swag)

        @app.route('/urls')
        def urls():
            return jsonify([str(p) for p in app.url_map.iter_rules()])

    return app


