from datetime import datetime
from flask_login import UserMixin
from sqlalchemy import Column, Integer, String, DateTime, DATETIME, text, Text, extract
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.schema import FetchedValue
from project.databases import Base
from project.utils.crypto import decrypt


class User(Base, UserMixin):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)  # uid
    name = Column(String(160))  # 이름(상점명)
    twitter_id = Column(String(100))  # 트위터 id
    enc_twitter_id = Column(String(200))  # 트위터 id (암호화)
    status = Column(Integer)  # 0:사용 1:탈퇴
    join_date = Column(DateTime)  # 가입일
    modified_at = Column(DateTime)
    password = Column(String(40))  # 암호
    enc_phone = Column(String(32))  # 전화번호 (암호화)
    hash_phone = Column(String(40))  # 전화번호 (해쉬)
    phone_hidden = Column(Integer, FetchedValue())  # 0:공개 1:숨김
    email = Column(String(100))  # 이메일
    hash_email = Column(String(40))  # 이메일 (해쉬)
    enc_email = Column(String(200))  # 이메일 (암호화)
    description = Column(Text)  # 상점설명
    favorite_count = Column(Integer, FetchedValue())  # 나를 팔로우 한사람
    comment_count = Column(Integer, FetchedValue())  # 댓글갯수(내상점)
    review_count = Column(Integer, FetchedValue())  # 내 별점 평가 사용자수
    grade = Column(Integer, FetchedValue())  # 사람들이 준 점수 합계(1인당 10점 만점)
    supporter_grade = Column(Integer, FetchedValue())  # 서포터즈 등급
    item_count = Column(Integer, FetchedValue())  # 내 물품 갯수
    item_price_sum = Column(Integer, FetchedValue())  # 내 물품 갯수 가격 합계
    interest = Column(Integer, FetchedValue())  # 내 상점 조회수
    noti_key = Column(String(72))  # 알림키(아이폰)
    noti_key_c2dm = Column(String(256))  # 알림키(안드로이드 c2dm)
    noti_cnt = Column(Integer, FetchedValue())
    udid = Column(String(40), FetchedValue())  # 단말기 식별코드
    device = Column(String(1))  # 단말기 정보(i:아이폰 a:안드로이드 w:웹)
    bizlicense = Column(Integer, FetchedValue())  # 전문판매자 여부(0:일반 1:전문, 5: 전문(인기상점태그부여))
    imgcnt = Column(Integer, FetchedValue())  # 프로필 사진 갯수(0:이미지가 없는 경우)
    privilege = Column(Integer, FetchedValue())  # 권한 (0:일반, 1:광고신청권한, 9:관리자)
    m_time = Column(Integer, default=0)
    img_location_s2 = Column(Integer, default=0)
    latest_interest = Column(Integer, FetchedValue())
    # 광고신청이 필요할경우 1로 변경되어 있는 상태여야 한다.

    STATUS_NORMAL_MEMBER = 0
    STATUS_WITHDRAWN_MEMBER = 1

    @property
    def num_grade_avg(self):
        if self.review_count == 0:
            return 0
        return str(int(round(float(self.grade) / self.review_count, 0)))

    @property
    def phone(self):
        return decrypt(self.enc_phone)

    @property
    def email(self):
        return decrypt(self.enc_email)

    class StatusCode(object):
        NORMAL, WITHDRAWN = 0, 1  # 정상, 탈퇴

    class BizLicenseCode(object):
        NORMAL, BIZ, BIZ_POPULAR = 0, 1, 5  # 일반, 전문상점, 전문상점(인기상점광고대상자)

    class PrivilegeCode(object):
        NORMAL_SELLER = 0  # 일반 판매자
        PREMIUM_SELLER = 1  # 전문 판매자
        OPERATION_ADMIN = 5  # 일반 운영자 (아르바이트)
        SUPER_ADMIN = 9  # 슈퍼 어드민

    @hybrid_property
    def join_date_day(self):
        return self.join_date.day

    @join_date_day.expression
    def join_date_day(cls):
        return extract('day', cls.join_date)

    @hybrid_property
    def join_date_month(self):
        return self.join_date.month

    @join_date_month.expression
    def join_date_month(cls):
        return extract('month', cls.join_date)


class UserReviewInfo(Base):
    __tablename__ = 'user_review_info'

    uid = Column(Integer, primary_key=True)
    review_count = Column(Integer, default=0)
    total_grade = Column(Integer, default=0)
    cert_review_count = Column(Integer, default=0)
    cert_total_grade = Column(Integer, default=0)
    modified_at = Column(DateTime, default=datetime.now, onupdate=datetime.now)


class UserAuthToken(Base):
    __tablename__ = 'user_auth_token'

    id = Column(Integer, primary_key=True)
    uid = Column(Integer)
    token = Column(String(64))
    policy_id = Column(Integer)  # 1.정상 2.제제


class UserImageUrl(Base):
    __tablename__ = 'user_image_url'

    uid = Column(Integer, primary_key=True)
    user_image_url = Column(String(120))
    created_at = Column(DATETIME, nullable=False, default=text('CURRENT_TIMESTAMP'))
    modified_at = Column(DATETIME)


class UserDevice(Base):
    __tablename__ = 'user_device'

    uid = Column(Integer, primary_key=True)
    source = Column(String(30))
    udid_str = Column(String(100))
    device = Column(String(1))
    device_model = Column(String(100))
    latest_version = Column(String(30))
    date = Column(DateTime)
    created_at = Column(DateTime)
    modified_at = Column(DateTime)
    status = Column(Integer)


class UserLastAccessTime(Base):
    __tablename__ = 'user_last_access_time'

    uid = Column(Integer, primary_key=True)
    access_time = Column(DateTime)


class UserRestrictionPolicy(Base):
    __tablename__ = 'user_restriction_policy'

    id = Column(Integer, primary_key=True, autoincrement=True)
    description = Column(String(100))
    profile_edit = Column(Integer, default=1)
    my_products_view = Column(Integer, default=1) # 내 물품 조회(0: disable, 1: enable)
    all_products_view = Column(Integer, default=1)  # 전체 물품 조회(0: disable, 1: enable)
    favorite_product = Column(Integer, default=1) # '물품 찜(0: disable, 1: enable)
    up_product = Column(Integer, default=1) # 물품 업(0: disable, 1: enable)
    contact_to_user = Column(Integer, default=1) # 연락하기(0: disable, 1: enable)
    contacted_by_user = Column(Integer, default=1) # 연락받기(0: disable, 1: enable)
    write_comment = Column(Integer, default=1) # 댓글 쓰기(0: disable, 1: enable)
    write_review = Column(Integer, default=1) # 리뷰 쓰기(0: disable, 1: enable)
    buntalk_read = Column(Integer, default=1) # 번톡 읽기(0: disable, 1: enable)
    buntalk_write = Column(Integer, default=1) # 번톡 쓰기(0: disable, 1: enable)
    ask_to_helpcenter = Column(Integer, default=1) # 상담센터 문의(0: disable, 1: enable)
    product_register = Column(Integer, default=1) # 물품 등록(0: disable, 1: enable)',
    product_modification = Column(Integer, default=1)  # '물품 수정(0: disable, 1: enable)',
    parcel_post = Column(Integer, default=1) # '택배 접수(0: disable, 1: enable)',
    parcel_tracking = Column(Integer, default=1) # '택배 조회(0: disable, 1: enable)',
    adcenter_access = Column(Integer, default=1) # '광고센터 접근(0: disable, 1: enable)',
    adcenter_request = Column(Integer, default=1) # '광고 신청(0: disable, 1: enable)',
    escrow_view = Column(Integer, default=1) # '안전결제 조회(0: disable, 1: enable)',
    product_escrow_modify = Column(Integer, default=1) # '물품 안전결제(네이버페이) 연동정보 수정(0: disable, 1: enable)',
    delete_account = Column(Integer, default=1) # '회원 탈퇴(0: disable, 1: enable)',
    default = Column(Integer, default=1) # '기본(0: disable, 1: enable)',
    buncar_dealer = Column(Integer)
    create_group = Column(Integer, default=1) # 그룹생성 (0: disable, 1: enable)'


class DeviceGroupBlockPolicyStatus(Base):
    __tablename__ = 'device_group_block_policy_status'

    id = Column(Integer, primary_key=True)  # uid
    udid = Column(String(100))  # user device id
    policy_id = Column(Integer, default=1)  # 적용된 차단정책. user_restriction_policy의 id
    created_at = Column(DATETIME, nullable=False, default=text('CURRENT_TIMESTAMP'))
    modified_at = Column(DATETIME, nullable=False, default=text('CURRENT_TIMESTAMP'))
    enc_udid = Column(String(256))


class BlockHistoryDeviceGroup(Base):
    __tablename__ = 'block_history_device_group'

    id = Column(Integer, primary_key=True, autoincrement=True)  # uid
    udid = Column(String(100))  # user device id
    offender_uid = Column(Integer)
    type = Column(Integer)
    block_end_date = Column(DateTime)
    reason = Column(String(200))
    policy_type1_category = Column(String(30))
    policy_type2_category = Column(String(30))
    victime_uid = Column(Integer)
    is_active = Column(Integer)
    writer_uid = Column(Integer)
    created_at = Column(DateTime)
    modified_at = Column(DateTime)
    enc_udid = Column(String(256))
    enc_reason = Column(Text)


class DeviceGroupException(Base):
    __tablename__ = 'device_group_exception'

    id = Column(Integer, primary_key=True, autoincrement=True)  # uid
    udid = Column(String(100))
    uid = Column(Integer)
    description = Column(Text)
    created_at = Column(DateTime)
    modified_at = Column(DateTime)
    enc_udid = Column(String(256))
