# decided to leave it for internal api.py
class ResponseType:
    SUCCESS = "success"
    BAD_REQUEST = "bad_request"
    UNAUTHORIZED = "unauthorized"
    BLOCKED = "blocked"
    FAIL = "fail"
    TOKEN_EXPIRED = "token_expired"
    SIGNUP_REQUIRED = "signup_required"
    ACCOUNT_DUPLICATED = "account_duplicated"
    BIZLICENSE = "bizlicense"

