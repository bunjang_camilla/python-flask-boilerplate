import sys
import base64
import hashlib
import random
import types

from Crypto.Cipher import AES as AESImpl


class CryptoKey(object):
    def __init__(self, bKey):
        self._key = bKey

    @property
    def keySize(self):
        return len(self._key)

    @property
    def alrogithm(self):
        raise NotImplementedError

    @property
    def blockSize(self):
        raise NotImplementedError

    @property
    def engine(self):
        raise NotImplementedError


class Padding(object):
    def __init__(self, block_size):
        BS = block_size
        self.pad = lambda s: s + (BS - len(s.encode('utf-8')) % BS) * chr(BS - len(s.encode('utf-8')) % BS)
        if sys.version_info > (2, 8):
            self.unpad = lambda s: s[0:-int(str(ord(s[-1])))]
        else:
            self.unpad = lambda s: s[0:-ord(s[-1])]


class Crypto(object):
    def __init__(self, cryptoKey, iv, padding_class=Padding):
        self._engine = cryptoKey.engine(iv)
        self._padding = padding_class(cryptoKey.blockSize)

    def encrypt(self, data):
        try:
            if type(data) == types.UnicodeType:
                data = data.encode('utf-8')
        except:
            pass
        return self._engine.encrypt(self._padding.pad(data))

    def b64encrypt(self, obj):
        return base64.b64encode(self.encrypt(obj))

    def decrypt(self, data):
        if sys.version_info > (2, 8):
            res = ''
            chunk = self._engine.decrypt(data)
            try:
                for i in chunk:
                    res += chr(i)
                    res = res.encode('utf-8')
            except:
                res = chunk.decode('utf-8')
            return str(self._padding.unpad(res))
        else:
            res = self._engine.decrypt(data)

        return self._padding.unpad(res)

    def b64decrypt(self, obj):
        return self.decrypt(base64.b64decode(obj))


class AESEngine(object):
    def __init__(self, key, iv):
        self._impl = AESImpl.new(key.encode,
                                 AESImpl.MODE_CBC,
                                 iv)
        self._blockSize = key.blockSize

    def encrypt(self, text):
        return self._impl.encrypt(text)

    def decrypt(self, bytes):
        return self._impl.decrypt(bytes)


class AESCryptoKey(CryptoKey):
    @property
    def encode(self):
        return self._key

    @property
    def alrogithm(self):
        return "AES"

    @property
    def blockSize(self):
        return 16

    def engine(self, iv):
        return AESEngine(self, iv)


'''
key list
'''
keys = ['wgXwQ+J0drK7T5tvQWDepw==', 'bGCdrpuYyF8egcZxup9HjQ==',
        'S3krS0NeBU0ySk/BnmbMTQ==', 'QtkjNd1HrCGBMS2XRXGZuw==',
        'UHofYADbaEt/inOPa6M/1g==', 'KiMzYSLx0afY9MpjIq6ZRg==',
        'LNMFG1eoXo4JePZqMWBoUw==', 'qEbTh5/VnXBesLdg38rNaw==',
        'OAY+bg7ADYZAtR7AKMMSKw==', 'CMKoimiSin7qNyfZVg+n4w==',
        'Bws7X2PpQ3Oo+ix7VhSBug==', 'imLKYRs+0XgM8uyZEJgmsA==',
        'j+gla1KxjjOlx6SBWl/K/g==', 'V0/ceT6PgIlQsysEy+VzxQ==',
        '03f4ucLRMJksnn32uxesdw==', 'lvNy1tOHORwygEbIgKfZuw==', ]

ivs = ['VEqVu17TwqFHWlzYLdAvDw==', 'bAZGGwXh08c34Bs6Sc97ow==',
       'kBa5mPzHK9oBJ7fr1hGacw==', 'xN6BARgtx4DYmLKT0g6Jyw==',
       'NHFNsqQzvvdtL/1eltd14g==', 'XLS6k1+6SM+skTNm3Z14Nw==',
       'mi0tciJerz5f8Q6WPVA3ew==', 'RFw/midsscRW02ZN4X3r+Q==',
       'M80vCqOxB3B3ZwcrZOrldw==', 'C79C+E8+YdonA3EHDQvqtg==',
       'um77VnAns5G7buSS4sVEYw==', 'yqigKflt+/ajmrz4/3O4lg==',
       'DT4MnNljfFUWfXIuVIKLmw==', '9IOGAbakI1GusIWwXCXUkg==',
       'nEeQVqSEjhUe6kWehX0dCg==', 'jFkcKuce/m240jItuwywUw==', ]


def encrypt(data):
    if not data:
        return data
    global keys
    key_index = random.randint(0, len(keys) - 1)
    key = base64.b64decode(keys[key_index])
    cryptoKey = AESCryptoKey(key)
    iv_index = random.randint(0, len(ivs) - 1)
    iv = base64.b64decode(ivs[iv_index])
    crypto = Crypto(cryptoKey, iv=iv)
    enc = crypto.b64encrypt(data)
    # <version>:<encrypted_text>:<key_index>:<iv_index>
    if sys.version_info > (2, 8):
        return '0:%s:%d:%d' % (str(enc)[2:-1], key_index, iv_index)
    return '0:%s:%d:%d' % (enc, key_index, iv_index)


def decrypt(data):
    if not data:
        return data
    global keys
    data = data.split(':')
    ver = int(data[0])
    if ver != 0:
        raise NotImplementedError
    key = base64.b64decode(keys[int(data[2])])
    cryptoKey = AESCryptoKey(key)
    iv = base64.b64decode(ivs[int(data[3])])
    crypto = Crypto(cryptoKey, iv=iv)
    return crypto.b64decrypt(data[1])


def sha1hex(data):
    return hashlib.sha1(data.encode('utf8')).hexdigest() if data else ''


def sha256hex(data):
    return hashlib.sha256(data).hexdigest() if data else ''


def sha512hex(data):
    return hashlib.sha512(data).hexdigest() if data else ''


def sha3_256hex(data):
    return hashlib.sha3_256(data).hexdigest() if data else ''

