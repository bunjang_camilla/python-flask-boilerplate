from flask import request, has_request_context, Blueprint
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from werkzeug.local import LocalProxy
from project.env import current_config

Write_Engine = create_engine(current_config.SERVICE_MASTER_DB_URI,
                             convert_unicode=True, pool_recycle=900, pool_size=10, max_overflow=15)
Read_Engine = create_engine(current_config.SERVICE_SLAVE_DB_URI,
                            convert_unicode=True, pool_recycle=900, pool_size=10, max_overflow=15)
WriteSession = sessionmaker(autocommit=False, autoflush=False, bind=Write_Engine)
ReadSession = sessionmaker(autocommit=False, autoflush=False, bind=Read_Engine)

Base = declarative_base()
db = Blueprint('db', __name__)


@LocalProxy
def read_session():
    if has_request_context():
        ctx = request._get_current_object()
    try:
        session_ = ctx._current_read_session
    except AttributeError:
        session_ = ReadSession()
        ctx._current_read_session = session_
    return session_


@LocalProxy
def write_session():
    if has_request_context():
        ctx = request._get_current_object()

    try:
        session_ = ctx._current_write_session
    except AttributeError:
        session_ = WriteSession()
        ctx._current_write_session = session_
    return session_


@db.teardown_app_request
def close_session(exception=None, *args, **kwargs):
    if has_request_context():
        ctx = request._get_current_object()
    else:
        ctx = args[1]

    if hasattr(ctx, '_current_read_session'):
        if exception is not None:
            ctx._current_read_session.rollback()
        ctx._current_read_session.close()

    if hasattr(ctx, '_current_write_session'):
        if exception is not None:
            ctx._current_write_session.rollback()
        ctx._current_write_session.close()
