from functools import wraps
from project.databases import read_session
from project.models import User
from flask import current_app, redirect, request, jsonify, g, make_response
from project.utils.service_enums import ResponseType
import jwt
#FIXME: 인증서버 생길때까지만 살려두기


SQL_FRAUD_USER_QUERY = """
    SELECT t.id, t.udid, t.privilege, t.status, 
    IF(sum(IF(target_policy=0, 1, 0)) > 0, 0, 1) AS target_policy, elapsed_day
       FROM
       (SELECT u.id, u.udid, u.privilege, u.status, p.{label} AS target_policy, DATEDIFF(NOW(), 
       uat.access_time) AS elapsed_day
       FROM user_restriction_policy AS p
       LEFT OUTER JOIN user_auth_token AS t ON t.policy_id = p.id
       LEFT OUTER JOIN `user` AS u ON u.id=t.uid
       LEFT OUTER JOIN user_last_access_time AS uat ON uat.uid=t.uid
       WHERE {where} 
       UNION ALL
       SELECT u.id, u.udid, u.privilege, u.status, p.{label} AS target_policy, 
       DATEDIFF(NOW(), uat.access_time) AS elapsed_day
       FROM user_restriction_policy AS p
       LEFT OUTER JOIN device_group_block_policy_status AS gps ON gps.policy_id = p.id
       LEFT OUTER JOIN `user` AS u ON u.id=({select_uid})
       LEFT OUTER JOIN user_last_access_time AS uat ON uat.uid=({select_uid})
       WHERE gps.udid IN 
        (SELECT udid
            FROM block_history_device_group
            WHERE udid IN 
                (SELECT ud.udid_str AS udid
                    FROM user_device AS ud 
                    LEFT OUTER JOIN device_group_exception AS dge ON ud.udid_str=dge.udid 
                    AND ud.uid=dge.uid 
                    WHERE ud.uid = ({select_uid})
                    AND ud.`status` = 0
                    AND ud.udid_str != ''
                    AND dge.uid IS NULL)
                    AND is_active=1
                    AND policy_type1_category IN ('거래사기')
                    GROUP BY udid)) AS t
"""

def is_master(uid):
    master_uid = {79, 27241, 9313, 53049, 250618, 64023, 10238, 32743, 15230, 234682, 20010, 323041, 3919669, 1018171,
                  128962, 4844997, 4922419, 224459}
    return uid in master_uid


def auth_by_token(label=None, required=True, admin_required=False):
    label = label or 'default'
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):

            params = request.args if request.method == 'GET' else request.form or (request.get_json() or {})  # request 에 아무것도 넘어오지 않는 케이스가 있음
            token = params.get('token', None)  # 구세대 토큰
            token = token if token else request.headers.get('X-Quicket-Auth-Token')
            token = token if token else request.args.get('session_token')

            result = None

            common_query = SQL_FRAUD_USER_QUERY

            if token:
                revised_token_method = token.startswith(current_app.config['JWT_KEY_PREFIX'])
                if revised_token_method:  # 새롭게 만들어진 토큰 방식인지 확인
                    try:
                        key_prefix_header_len = len(current_app.config['JWT_KEY_PREFIX'])
                        payload_decoded = jwt.decode(token[key_prefix_header_len:],
                                                     current_app.config['JWT_SECRET'],
                                                     algorithms=['HS256'],
                                                     verify=True)
                    except jwt.exceptions.ExpiredSignatureError:
                        # FIXME: 일단 JWT가 클라이언트에 안착될때 까지 verify=False 로 만들자
                        # 시간이 만료된 경우에만이라도 허용하자
                        payload_decoded = jwt.decode(token[key_prefix_header_len:],
                                                     current_app.config['JWT_SECRET'],
                                                     algorithms=['HS256'], verify=False)
                        # return jsonify(result=ResponseType.TOKEN_EXPIRED, reason='다시 로그인 해주세요')

                    query_where = 'u.id=:uid'
                    query_select_uid = ':uid'
                    query = common_query.format(label=label, where=query_where, select_uid=query_select_uid)
                    uid = payload_decoded['uid']
                    if uid != '':
                        query_params = {'uid': uid}
                        result = read_session.execute(query, query_params).fetchone()
                else:
                    query_where = "t.token=:token"
                    query_select_uid = "SELECT uid FROM user_auth_token WHERE token=:token"
                    query = common_query.format(label=label, where=query_where, select_uid=query_select_uid)
                    query_params = {'token': token}

                    result = read_session.execute(query, query_params).fetchone()

            if result and result.id is not None:
                if admin_required:
                    if result.privilege < 5:
                        return make_response(jsonify(result=ResponseType.UNAUTHORIZED,
                                                     reason='권한 레벨이 낮습니다.'), 401)

                g.uid = result.id
                g.udid = result.udid
                g.privilege = result.privilege
                g.user_status = result.status
                '''
                if kwargs.has_key('uid') and str(uid_from_token) != str(kwargs['uid']):
                    return jsonify(result=ResponseType.UNAUTHORIZED,
                                   reason="Not matched 'uid' parameter")
                '''
                # 'product_modification'
                if label in ('contact_to_user', 'write_comment', 'write_review', 'product_register'):
                    reported_cnt = read_session.execute("""
                            SELECT COUNT(DISTINCT uid) cnt
                            FROM emergency LEFT JOIN user ON emergency.target_uid=user.id
                            WHERE time > DATE_SUB(NOW(), INTERVAL 1 DAY) AND target_uid=:uid AND uid!=target_uid 
                            AND etc_reason != 'CS관리자 삭제'
                        """, {'uid': g.uid}).scalar()
                    if reported_cnt and reported_cnt >= 4 and not is_master(int(g.uid)):
                        return make_response(jsonify(result=ResponseType.FAIL,
                                                     reason="최근 24시간 동안 다수의 사용자들로 부터 신고를 받으셨습니다. "
                                                            "등록 기능이 자동으로 정지됩니다. "
                                                            "허위 신고 관련 문의는 "
                                                            "[설정 > 1:1 상담하기] 메뉴를 이용해주세요."), 403)

                # 'is_sprint_admin' 태그가 있다면 탈퇴한 사용자도 허가한다.
                if (not params.get('is_sprint_admin') and g.user_status == User.STATUS_WITHDRAWN_MEMBER):
                    return make_response(jsonify(result=ResponseType.UNAUTHORIZED), 401)

                if not result.target_policy and label != 'buncar_dealer':
                    # TODO-shjeong: 차단, 기기그룹적용할 때 광고개시중 상품 소유한 사용자는 제외한다
                    return make_response(jsonify(result=ResponseType.BLOCKED,
                                                 reason="이용이 제한된 유저로 일부 기능이 제한됩니다. "
                                                        "관련 문의는 [설정 > 1:1 상담하기] 메뉴를 이용해주세요."), 401)

            elif required:
                return make_response(jsonify(result=ResponseType.UNAUTHORIZED), 401)
            return f(*args, **kwargs)
        return decorated_function
    return decorator


def ssl_required(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        if current_app.config.get("SSL"):
            if request.is_secure:
                return fn(*args, **kwargs)
            else:
                return redirect(request.url.replace("http://", "https://"))
        return fn(*args, **kwargs)
    return decorated_view

