from flask_login import UserMixin, AnonymousUserMixin


class User(UserMixin):
    level = "normal"

    def __init__(self, name, id, profile_image='', level="0:normal", active=True):
        super(User, self).__init__()
        self.name = name
        self.id = id
        self.active = active
        self.profile_image = profile_image
        # python3에서는 long이 int로 변경
        # if isinstance(level, (int, long)):
        if isinstance(level, (int, int)):
            if level == 9:
                self.level = '9:admin'
            elif level >= 7:
                self.level = '7:operator'
            elif level >= 5:
                self.level = '5:partTime'
            elif level >= 1:
                self.level = '1:business'
            else:
                self.level = '0:normal'
        else:
            self.level = level

    def is_active(self):
        print(self.level)
        return self.active

    def is_admin(self):
        print(self.level)
        return self.level == '9:admin'

    def is_operator(self):
        print(self.level)
        return self.level >= '7:operator'

    def is_part_time(self):
        print(self.level)
        return self.level >= '5:partTime'

    def is_business(self):
        print(self.level)
        return self.level >= '1:business'


class Anonymous(AnonymousUserMixin, User):

    def __init__(self):
        super(Anonymous, self).__init__(name='', id=-1, profile_image='', active=False)

USERLIST = {}