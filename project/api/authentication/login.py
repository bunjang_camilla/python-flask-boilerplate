from flask import request, redirect, render_template, jsonify, session, make_response
from flask.views import MethodView
from flask_login import current_user, login_user, logout_user, confirm_login
from project.api import authentication
from project.api.authentication.decorators import ssl_required
from project.api.services.core_api import CoreAPI
from project.databases import read_session
from project.models import UserAuthToken
from project.utils.service_enums import ResponseType
# FIXME: 로그인 사용 안함


class Token(MethodView):
    @ssl_required
    def post(self):
        """
        토큰검사
        ---
        parameters:
          - in: body
            name: body
            required:
                - next
                - access_token
        responses:
          200:
            description: OK
        """
        logout_user()
        data = request.form.to_dict()
        required_fields = 'next', 'access_token'
        for name in required_fields:
            if not data.get(name):
                return make_response(jsonify(data=f'{name} is missing'), 400)

        uid = self.get_uid(data['access_token'], 1)
        if uid:
            user_info = self.get_user_info(uid)
            login_user(authentication.User(user_info.name, uid, user_info.user_image_url))
            confirm_login()
            return redirect(data['next'])
        return redirect('/')

    def get_user_info(self, uid):
        query = """
                SELECT u.id, u.name, u.enc_phone, u.interest, u.privilege, ui.user_image_url
                 FROM user u
                 LEFT JOIN user_image_url ui ON u.id= ui.uid
                 WHERE u.id = :uid
        """
        return read_session.execute(query, dict(uid=uid)).fetchone()

    def get_uid(self, token, policy_id):
        token = read_session.query(UserAuthToken.uid) \
            .filter(UserAuthToken.token == token,
                    UserAuthToken.policy_id == policy_id) \
            .scalar()
        return token


class Login(MethodView):
    @ssl_required
    def post(self):
        next_url = '/'
        login_url = 'membership/login.html'

        # 이미 로그인이 되어 있으면 url로 리다이렉트
        if current_user.is_authenticated:
            session.permanent = True
            return redirect(next_url)

        data = request.form.to_dict()
        # 아이디 비밀번호 입력
        user_id = data.get('userid', None)
        user_pw = data.get('userpw', None)

        # 핸드폰번호 및 패스워드 검증c
        if not user_id or not user_pw:
            return render_template(login_url, next=next_url)

        # 로그인 API 호출
        core = CoreAPI()
        res = core.login(user_id, user_pw)

        # 각각의 인증 상태값에 따라 분기
        if res['result'] == 'success':
            session.permanent = True
            user_info = self.get_user_info(res['uid'])
            logout_user()
            login_user(authentication.User(user_info.name, res['uid'], user_info.user_image_url), remember=True)
            confirm_login()
        elif res['result'] == 'unauthorized':
            return make_response(jsonify(dict(result=ResponseType.UNAUTHORIZED)), 401)

        elif res['result'] == 'blocked':
            return make_response(jsonify(dict(result=ResponseType.BLOCKED)), 403)

        if 'next' in request.args:
            next_url = data.get('next')
        if current_user.is_authenticated:
            return jsonify({'result': ResponseType.SUCCESS, 'next': next_url})
        return render_template(login_url, next=next_url)


