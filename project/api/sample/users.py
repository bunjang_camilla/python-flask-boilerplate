from flask import g, make_response, jsonify, request
from flask_restful import Resource
from project.api.authentication.decorators import auth_by_token
from project.databases import read_session, write_session
from project.models import User


class UserDetail(Resource):
    @auth_by_token()
    def get(self):
        """
        유저 상세정보
        ---
        responses:
          200:
            description: 유저 상세정보
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    id:
                      type: integer
                      description: 유저 아이디
                    name:
                      type: string
                      description: 유저 이름
        """
        user = read_session.query(User).filter(User.id == g.uid).first()
        return jsonify(data=dict(id=user.id, name=user.name))

    @auth_by_token()
    def patch(self):
        """
        유저 상세정보 업데이트
        ---
        parameters:
          - in: body
            required:
                - name
                - description
        responses:
          422:
            description: parameter is missing
          201:
            description: 유저 업데이트된 새 정보
            content:
              application/json:
                schema:
                  type: object
                  properties:
                    id:
                      type: integer
                      description: 유저 아이디
                    name:
                      type: string
                      description: 유저 이름
                    description:
                      type: string
                      description: 유저 설명
        """
        data = request.get_json(silent=True)
        required_fields = 'name', 'description'
        for name in required_fields:
            if not data.get(name):
                return make_response(jsonify(data=f'{name} is missing'), 422)
        user = write_session.query(User).filter(User.id == g.uid).first()
        user.name = data['name']
        user.description = data['description']
        write_session.commit()
        return jsonify(data=dict(id=user.id, name=user.name, description=user.description))
