import requests
from project.env import current_config
from project.utils.crypto import sha1hex


class CoreAPI:
    def __init__(self):
        self.CORE = current_config.BUNJANG_DOMAIN_FOR_API

    def login(self, phone, password):
        url = f'http://{self.CORE}/api/1/auth/access_token'
        response = requests.post(url, data={'userid': phone,
                                            'userpw': sha1hex(password)}).json()
        return response
